<?php

use \Doctrine\Common\Annotations\AnnotationRegistry;

// Composer
if (file_exists(__DIR__.'/../vendor/autoload.php')) {
    $loader = require_once __DIR__.'/../vendor/autoload.php';

    \Phake::setClient(Phake::CLIENT_PHPUNIT);

    //AnnotationRegistry::registerLoader(array($loader, 'loadClass'));
    return $loader;
}

throw new \RuntimeException('Could not find vendor/autoload.php, make sure you ran composer.');
