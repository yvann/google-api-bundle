<?php

namespace Yvann\GoogleAPIBundle\Tests\Functional;

use \Yvann\GoogleAPIBundle\Model\Base\LatLng,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Radar\Request,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Radar\Response,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Radar\Service;

/**
 * @group functional
 */
class PlacesSearchRadarTest extends WebTestCase
{
    public function testServiceDefaultValues()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Service $service */
        $service = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.radar.service');

        $this->assertInstanceOf('Yvann\GoogleAPIBundle\Service\Places\Search\Radar\Service', $service);
        $this->assertEquals('https://maps.googleapis.com/maps/api/place/radarsearch', (string) $service->getUrl());
    }

    public function testRequestDefaultValues()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Request $request */
        $request = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.radar.request');

        $this->assertInstanceOf('Yvann\GoogleAPIBundle\Service\Places\Search\Radar\Request', $request);

        $this->assertEquals('AIzaSyC0L90v-j9G1VmszVJDi6aFl2MzqLhDqRA', $request->getKey());
        $this->assertEquals('en', $request->getLanguage());
        $this->assertFalse($request->hasSensor());
    }

    public function testValidExecution()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Request $request */
        $request = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.radar.request')
            ->setRadius(5000)
            ->setLocation(new LatLng(48.856578, 2.351828))
            ->setTypes(array('bar'))
        ;

        /** @var Service $service */
        $service = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.radar.service');

        /** @var Response $response */
        $response = $service->execute($request);

        $this->assertEquals('OK', $response->getStatus());
        $this->assertCount(200, $response->getPlaces());
        foreach ($response->getPlaces() as $placeResult) {
            $this->assertInstanceOf('Yvann\GoogleAPIBundle\Model\Places\PlaceResult', $placeResult);

            $this->assertEmpty($placeResult->getId());
            $this->assertEmpty($placeResult->getName());

            $this->assertNotEmpty($placeResult->getGeometry());
            $this->assertNotEmpty($placeResult->getGeometry()->getLocation());
            $this->assertNotEmpty($placeResult->getGeometry()->getLocation()->getLatitude());
            $this->assertNotEmpty($placeResult->getGeometry()->getLocation()->getLongitude());

            $this->assertNotEmpty($placeResult->getReference());
        }
    }
}
