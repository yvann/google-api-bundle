<?php

namespace Yvann\GoogleAPIBundle\Tests\Functional;

use \Yvann\GoogleAPIBundle\Model\Base\LatLng,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Request,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Response,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Service;

/**
 * @group functional
 */
class PlacesSearchNearbyTest extends WebTestCase
{
    public function testServiceDefaultValues()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Service $service */
        $service = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.nearby.service');

        $this->assertInstanceOf('Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Service', $service);
        $this->assertEquals('https://maps.googleapis.com/maps/api/place/nearbysearch', (string) $service->getUrl());
    }

    public function testRequestDefaultValues()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Request $request */
        $request = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.nearby.request');

        $this->assertInstanceOf('Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Request', $request);

        $this->assertEquals('AIzaSyC0L90v-j9G1VmszVJDi6aFl2MzqLhDqRA', $request->getKey());
        $this->assertEquals('en', $request->getLanguage());
        $this->assertFalse($request->hasSensor());
    }

    public function testValidExecution()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Request $request */
        $request = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.nearby.request')
            ->setRadius(5000)
//            ->setLocation(new LatLng(48.856578, 2.351828))
            ->setLocation(new LatLng(40.758895, -73.985131))
            ->setTypes(array('bar'))
            ->setExtended(false)
        ;

        /** @var Service $service */
        $service = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.nearby.service');

        /** @var Response $response */
        $response = $service->execute($request);

        $this->assertEquals('OK', $response->getStatus());
        $this->assertCount(20, $response->getPlaces());
        foreach ($response->getPlaces() as $placeResult) {
            $this->assertInstanceOf('Yvann\GoogleAPIBundle\Model\Places\PlaceResult', $placeResult);

            $this->assertNotEmpty($placeResult->getId());
            $this->assertNotEmpty($placeResult->getReference());
            $this->assertNotEmpty($placeResult->getName());

            $this->assertTrue($placeResult->hasType('bar'));
        }
    }

    public function testMultipleValidExecutions()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Request $request */
        $request = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.nearby.request')
            ->setRadius(5000)
//            ->setLocation(new LatLng(48.856578, 2.351828))
            ->setLocation(new LatLng(40.758895, -73.985131))
            ->setTypes(array('bar'))
            ->setExtended(true)
        ;

        /** @var Service $service */
        $service = static::$kernel->getContainer()->get('yvann_google_api.service.places.search.nearby.service');

        /** @var Response $response */
        $response = $service->execute($request);

        $this->assertEquals('OK', $response->getStatus());
        $this->assertCount(60, $response->getPlaces());
        foreach ($response->getPlaces() as $placeResult) {
            $this->assertInstanceOf('Yvann\GoogleAPIBundle\Model\Places\PlaceResult', $placeResult);

            $this->assertNotEmpty($placeResult->getId());
            $this->assertNotEmpty($placeResult->getReference());
            $this->assertNotEmpty($placeResult->getName());

            $this->assertTrue($placeResult->hasType('bar'));
        }

        $detailsResponse = static::$kernel->getContainer()->get('yvann_google_api.service.places.details.service')->execute(
            static::$kernel->getContainer()->get('yvann_google_api.service.places.details.request')
                ->setReference($placeResult->getReference())
        );

        $this->assertNotEmpty($detailsResponse->getPlace()->getFormattedAddress());
    }
}
