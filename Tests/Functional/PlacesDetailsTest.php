<?php

namespace Yvann\GoogleAPIBundle\Tests\Functional;

use \Yvann\GoogleAPIBundle\Model\Base\LatLng,
    \Yvann\GoogleAPIBundle\Service\Places\Details\Request,
    \Yvann\GoogleAPIBundle\Service\Places\Details\Response,
    \Yvann\GoogleAPIBundle\Service\Places\Details\Service;

/**
 * @group functional
 */
class PlacesDetailsTest extends WebTestCase
{
    public function testServiceDefaultValues()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Service $service */
        $service = static::$kernel->getContainer()->get('yvann_google_api.service.places.details.service');

        $this->assertInstanceOf('Yvann\GoogleAPIBundle\Service\Places\Details\Service', $service);
        $this->assertEquals('https://maps.googleapis.com/maps/api/place/details', (string) $service->getUrl());
    }

    public function testRequestDefaultValues()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Request $request */
        $request = static::$kernel->getContainer()->get('yvann_google_api.service.places.details.request');

        $this->assertInstanceOf('Yvann\GoogleAPIBundle\Service\Places\Details\Request', $request);

        $this->assertEquals('AIzaSyC0L90v-j9G1VmszVJDi6aFl2MzqLhDqRA', $request->getKey());
        $this->assertEquals('en', $request->getLanguage());
        $this->assertFalse($request->hasSensor());
    }

    public function testValidExecution()
    {
        static::$kernel = static::createKernel(array());
        static::$kernel->boot();

        /** @var Request $request */
        $request = static::$kernel->getContainer()->get('yvann_google_api.service.places.details.request')
            ->setReference('CnRrAAAAjdCHqnrkT8NJvcGI9-7ioHJeg94sU9zRQ-h06vKMKr03dVKVL6ZExjAXT_s6SUCbzKDgB1dRpc8a5k7NmYM0iFUv0rCgzbkx1GaPEcMQfPefvGa-TgYySA06dpPQCess0ZrYoa0exKVIzsFIYIaAVBIQoldrQA6pTNDo7BAiGwv2xhoUpxLZE-qMBvRQP4qI3gUK8qPzJVM')
        ;

        /** @var Service $service */
        $service = static::$kernel->getContainer()->get('yvann_google_api.service.places.details.service');

        /** @var Response $response */
        $response = $service->execute($request);

        $this->assertInstanceOf('Yvann\GoogleAPIBundle\Model\Places\PlaceResult', $response->getPlace());

        $placeResult = $response->getPlace();

        $this->assertNotEmpty($placeResult->getId());
        $this->assertNotEmpty($placeResult->getReference());
        $this->assertNotEmpty($placeResult->getName());
    }
}
