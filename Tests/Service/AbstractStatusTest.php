<?php

namespace Yvann\GoogleAPIBundle\Tests\Service;

use \Yvann\GoogleAPIBundle\Service\AbstractStatus;

/**
 * Status of the request
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
class AbstractStatusTest extends \PHPUnit_Framework_TestCase
{
    public function testGetStatuses()
    {
        $this->assertEquals([
            'OK',
            'ZERO_RESULTS',
            'OVER_QUERY_LIMIT',
            'REQUEST_DENIED',
            'INVALID_REQUEST',
            'UNKNOWN_ERROR',
        ], AbstractStatus::getStatuses());
    }
}
