<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places;

use \Yvann\GoogleAPIBundle\Tests\Service\AbstractStatusTest as BaseStatus;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
class AbstractStatusTest extends BaseStatus
{
}
