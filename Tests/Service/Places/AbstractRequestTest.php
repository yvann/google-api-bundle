<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places;

use \Yvann\GoogleAPIBundle\Tests\Service\AbstractRequestTest as BaseRequest;

/**
 * Request test
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class AbstractRequestTest extends BaseRequest
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->request = \Phake::partialMock('\Yvann\GoogleAPIBundle\Service\Places\AbstractRequest', false, 'azerty123456789', 'xy');
    }
}
