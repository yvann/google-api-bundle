<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places\Search;

use \Yvann\GoogleAPIBundle\Tests\Service\Places\AbstractRequestTest as BaseRequest;

/**
 * Request test
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class AbstractRequestTest extends BaseRequest
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->request = \Phake::partialMock('\Yvann\GoogleAPIBundle\Service\Places\Search\AbstractRequest', false, 'azerty123456789', 'xy');

        $this->location = \Phake::mock('\Yvann\GoogleAPIBundle\Model\Base\LatLngInterface');
        \Phake::when($this->location)->getLatitude()->thenReturn(1.23456789);
        \Phake::when($this->location)->getLongitude()->thenReturn(9.87654321);

        $this->priceRange = \Phake::mock('\Yvann\GoogleAPIBundle\Model\Places\PlacePriceRange');
        \Phake::when($this->priceRange)->getMin()->thenReturn(1);
        \Phake::when($this->priceRange)->getMax()->thenReturn(3);
    }

    public function testConstruct()
    {
        parent::testConstruct();

        $this->assertInstanceOf('\Yvann\GoogleAPIBundle\Model\Places\PlacePriceRange', $this->request->getPriceRange());
        $this->assertNull($this->request->getPriceRange()->getMin());
        $this->assertNull($this->request->getPriceRange()->getMax());
    }

    public function testLocation()
    {
        $this->assertNull($this->request->getLocation());

        $this->request->setLocation($this->location);
        $this->assertEquals(1.23456789, $this->request->getLocation()->getLatitude());
        $this->assertEquals(9.87654321, $this->request->getLocation()->getLongitude());
    }

    public function testRadius()
    {
        $this->assertNull($this->request->getRadius());

        $this->request->setRadius(1234);
        $this->assertEquals(1234, $this->request->getRadius());

        $this->setExpectedException('\InvalidArgumentException');
        $this->request->setRadius('1');
    }

    public function testKeyword()
    {
        $this->assertNull($this->request->getKeyword());

        $this->request->setKeyword('cheap bar');
        $this->assertEquals('cheap bar', $this->request->getKeyword());
    }

    public function testName()
    {
        $this->assertNull($this->request->getName());

        $this->request->setName('Xème');
        $this->assertEquals('Xème', $this->request->getName());
    }

    public function testTypes()
    {
        $this->assertEmpty($this->request->getTypes());

        $this->request->setTypes(array('bar', 'restaurant'));
        $this->assertEquals(array('bar', 'restaurant'), $this->request->getTypes());

        $this->request->addTypes(array('accounting', 'atm'));
        $this->assertEquals(array('bar', 'restaurant', 'accounting', 'atm'), $this->request->getTypes());

        $this->request->addType('fire_station');
        $this->assertEquals(array('bar', 'restaurant', 'accounting', 'atm', 'fire_station'), $this->request->getTypes());
    }

    public function testPriceRange()
    {
        $this->assertNull($this->request->getPriceRange()->getMin());
        $this->assertNull($this->request->getPriceRange()->getMax());

        $this->request->setPriceRange($this->priceRange);
        $this->assertEquals(1, $this->request->getPriceRange()->getMin());
        $this->assertEquals(3, $this->request->getPriceRange()->getMax());
    }

    public function testOpenNow()
    {
        $this->assertNull($this->request->getOpenNow());

        $this->request->setOpenNow(false);
        $this->assertFalse($this->request->getOpenNow());

        $this->request->setOpenNow(true);
        $this->assertTrue($this->request->getOpenNow());
    }

    public function testZagatSelected()
    {
        $this->assertNull($this->request->getZagatSelected());

        $this->request->setZagatSelected(false);
        $this->assertFalse($this->request->getZagatSelected());

        $this->request->setZagatSelected(true);
        $this->assertTrue($this->request->getZagatSelected());
    }
}
