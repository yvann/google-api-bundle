<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places\Search\Radar;

use \Yvann\GoogleAPIBundle\Tests\Service\Places\Search\AbstractStatusTest as BaseStatus,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Radar\Status;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
class StatusTest extends BaseStatus
{
    public function testConstructor()
    {
        $this->setExpectedException('\Exception');
        $status = new Status();
    }
}
