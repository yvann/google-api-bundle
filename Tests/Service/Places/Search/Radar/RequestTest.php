<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places\Search\Radar;

use \Yvann\GoogleAPIBundle\Tests\Service\Places\Search\AbstractRequestTest as BaseRequest;

/**
 * Request test
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class RequestTest extends BaseRequest
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->request = new \Yvann\GoogleAPIBundle\Service\Places\Search\Radar\Request(false, 'azerty123456789', 'xy');
    }

    public function testRadius()
    {
        $this->assertEquals(50000, $this->request->getRadius());

        $this->request->setRadius(1234);
        $this->assertEquals(1234, $this->request->getRadius());

        $this->setExpectedException('\InvalidArgumentException');
        $this->request->setRadius('1');
    }

    public function testIsValid()
    {
        $this->assertFalse($this->request->isValid());

        $this->request->setLocation($this->location);
        $this->assertFalse($this->request->isValid());

        $this->request->setRadius(50000);
        $this->assertFalse($this->request->isValid());

        $this->request->setKeyword('bar');
        $this->assertTrue($this->request->isValid());
    }
}
