<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places\Search\Nearby;

use \Yvann\GoogleAPIBundle\Tests\Service\Places\Search\AbstractRequestTest as BaseRequest;

/**
 * Request test
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class RequestTest extends BaseRequest
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->request = new \Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Request(false, 'azerty123456789', 'xy');
    }

    public function testConstruct()
    {
        parent::testConstruct();

        $this->assertEquals('prominence', $this->request->getRankBy());
        $this->assertNull($this->request->getPageToken());
    }

    public function testPagetoken()
    {
        $this->assertNull($this->request->getPagetoken());

        $this->request->setPagetoken('aaaaaaazzzzzzzeeeeerrrrrrrt!t!!reg!trhntyùmntrqùn,');
        $this->assertEquals('aaaaaaazzzzzzzeeeeerrrrrrrt!t!!reg!trhntyùmntrqùn,', $this->request->getPagetoken());
    }

    public function testIsValid()
    {
        $this->assertFalse($this->request->isValid());

        $this->request->setLocation($this->location);
        $this->assertFalse($this->request->isValid());

        $this->request->setRadius(1234);
        $this->assertTrue($this->request->isValid());
    }
}
