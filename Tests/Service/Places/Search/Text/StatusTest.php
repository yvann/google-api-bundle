<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places\Search\Text;

use \Yvann\GoogleAPIBundle\Tests\Service\Places\Search\AbstractStatusTest as BaseStatus,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Text\Status;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
class StatusTest extends BaseStatus
{
    public function testConstructor()
    {
        $this->setExpectedException('\Exception');
        $status = new Status();
    }
}
