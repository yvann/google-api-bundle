<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places\Search\Text;

use \Yvann\GoogleAPIBundle\Tests\Service\Places\Search\AbstractRequestTest as BaseRequest;

/**
 * Request test
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class RequestTest extends BaseRequest
{
    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->request = new \Yvann\GoogleAPIBundle\Service\Places\Search\Text\Request(false, 'azerty123456789', 'xy');
    }

    public function testKeyword()
    {
        $this->setExpectedException('\BadMethodCallException');
        $this->request->setKeyword('cheap bar');
    }

    public function testName()
    {
        $this->setExpectedException('\BadMethodCallException');
        $this->request->setName('Xème');
    }

    public function testIsValid()
    {
        $this->assertFalse($this->request->isValid());

        $this->request->setQuery('bar');
        $this->assertTrue($this->request->isValid());
    }
}
