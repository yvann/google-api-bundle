<?php

namespace Yvann\GoogleAPIBundle\Tests\Service\Places\Search;

use \Yvann\GoogleAPIBundle\Tests\Service\Places\AbstractStatusTest as BaseStatus;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
class AbstractStatusTest extends BaseStatus
{
}
