<?php

namespace Yvann\GoogleAPIBundle\Tests\Service;

/**
 * Request test
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class AbstractRequestTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var mixed Request tested
     */
    protected $request = null;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->request = \Phake::partialMock('\Yvann\GoogleAPIBundle\Service\AbstractRequest', false, 'azerty123456789', 'xy');
    }

    public function testConstruct()
    {
        $this->assertFalse($this->request->hasSensor());
        $this->assertEquals('azerty123456789', $this->request->getKey());
        $this->assertEquals('xy', $this->request->getLanguage());
    }

    public function testSensor()
    {
        $this->assertFalse($this->request->hasSensor());

        $this->request->setSensor(false);
        $this->assertFalse($this->request->hasSensor());

        $this->request->setSensor(true);
        $this->assertTrue($this->request->hasSensor());

        $this->setExpectedException('\InvalidArgumentException');
        $this->request->setSensor('0');
    }

    public function testKey()
    {
        $this->assertEquals('azerty123456789', $this->request->getKey());

        $this->request->setKey('987654321ytreza');
        $this->assertEquals('987654321ytreza', $this->request->getKey());

        $this->setExpectedException('\InvalidArgumentException');
        $this->request->setKey('');
    }

    public function testLanguage()
    {
        $this->assertEquals('xy', $this->request->getLanguage());

        $this->request->setLanguage('fr');
        $this->assertEquals('fr', $this->request->getLanguage());
    }

    public function testIsValid()
    {
        $this->assertTrue($this->request->isValid());
    }
}
