<?php

namespace Yvann\GoogleAPIBundle\Event;

use \Symfony\Component\EventDispatcher\Event;

use \Yvann\GoogleAPIBundle\Service\AbstractService,
    \Yvann\GoogleAPIBundle\Service\AbstractRequest,
    \Yvann\GoogleAPIBundle\Service\AbstractResponse;

/**
 * Represents an event concerning the Google API
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
class GoogleAPIEvent extends Event
{
    /**
     * @var AbstractService
     */
    protected $service;

    /**
     * @var AbstractRequest
     */
    protected $request;

    /**
     * @var array
     */
    protected $providerRequestParameters;

    /**
     * @var string
     */
    protected $providerRequest;

    /**
     * @var string
     */
    protected $providerResponse;

    /**
     * @var AbstractResponse
     */
    protected $response;

    public function __construct(
        AbstractService $service = null,
        AbstractRequest $request = null,
        array $providerRequestParameters = null,
        $providerRequest = null,
        $providerResponse = null,
        AbstractResponse $response = null
    ) {
        $this->service = $service;
        $this->request = $request;
        $this->providerRequestParameters = $providerRequestParameters;
        $this->providerRequest = $providerRequest;
        $this->providerResponse = $providerResponse;
        $this->response = $response;
    }

    /**
     * @return \Yvann\GoogleAPIBundle\Service\AbstractService
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * @return \Yvann\GoogleAPIBundle\Service\AbstractRequest
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return array
     */
    public function getProviderRequestParameters()
    {
        return $this->providerRequestParameters;
    }

    /**
     * @return string
     */
    public function getProviderRequest()
    {
        return $this->providerRequest;
    }

    /**
     * @return string
     */
    public function getProviderResponse()
    {
        return $this->providerResponse;
    }

    /**
     * @return \Yvann\GoogleAPIBundle\Service\AbstractResponse
     */
    public function getResponse()
    {
        return $this->response;
    }
}
