<?php

namespace Yvann\GoogleAPIBundle\Service\Places;

use \Yvann\GoogleAPIBundle\Service\AbstractResponse as BaseResponse;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractResponse extends BaseResponse
{
    /**
     * @var array contain a set of attributions about this listing which must be displayed to the user.
     */
    protected $htmlAttributions;

    /**
     * Gets the place search response html attribution
     *
     * @return array
     */
    public function getHtmlAttributions()
    {
        return $this->htmlAttributions;
    }

    /**
     * Sets the place search response html attribution
     *
     * @param  array            $htmlAttributions
     * @return AbstractResponse
     */
    public function setHtmlAttributions(array $htmlAttributions)
    {
        $this->htmlAttributions = array();
        foreach ($htmlAttributions as $htmlAttribution) {
            $this->addHtmlAttribution($htmlAttribution);
        }

        return $this;
    }

    /**
     * Add a place search response html attribution
     *
     * @param  string           $htmlAttribution
     * @return AbstractResponse
     */
    public function addHtmlAttribution($htmlAttribution)
    {
        $this->htmlAttributions[] = $htmlAttribution;

        return $this;
    }
}
