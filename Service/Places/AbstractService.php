<?php

namespace Yvann\GoogleAPIBundle\Service\Places;

use \Yvann\GoogleAPIBundle\Service\AbstractService as BaseService,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceResult,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceGeometry,
    \Yvann\GoogleAPIBundle\Model\Geocoder\GeocoderAddressComponents,
    \Yvann\GoogleAPIBundle\Model\Geocoder\GeocoderAddressComponent,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceReviews,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceReview,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceAspectRatings,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceAspectRating,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceEvents,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceEvent;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractService extends BaseService
{
    /**
     * @param  \stdClass   $providerPlaceResult
     * @param  PlaceResult $placeResult
     * @return PlaceResult
     */
    protected function populatePlaceResult($providerPlaceResult, PlaceResult $placeResult = null)
    {
        if (null === $placeResult) {
            $placeResult = new PlaceResult();
        }

        !isset($providerPlaceResult->id) ?:                         $placeResult->setId($providerPlaceResult->id);
        !isset($providerPlaceResult->reference) ?:                  $placeResult->setReference($providerPlaceResult->reference);
        !isset($providerPlaceResult->icon) ?:                       $placeResult->setIcon($providerPlaceResult->icon);
        !isset($providerPlaceResult->name) ?:                       $placeResult->setName($providerPlaceResult->name);
        !isset($providerPlaceResult->types) ?:                      $placeResult->setTypes($providerPlaceResult->types);
        !isset($providerPlaceResult->address_components) ?:         $placeResult->setAddressComponents($this->populateGeocoderAddressComponents($providerPlaceResult->address_components));
        !isset($providerPlaceResult->vicinity) ?:                   $placeResult->setVicinity($providerPlaceResult->vicinity);
        !isset($providerPlaceResult->formatted_address) ?:          $placeResult->setFormattedAddress($providerPlaceResult->formatted_address);
        !isset($providerPlaceResult->formatted_phone_number) ?:     $placeResult->setFormattedPhoneNumber($providerPlaceResult->formatted_phone_number);
        !isset($providerPlaceResult->international_phone_number) ?: $placeResult->setInternationalPhoneNumber($providerPlaceResult->international_phone_number);
        !isset($providerPlaceResult->rating) ?:                     $placeResult->setRating($providerPlaceResult->rating);
        !isset($providerPlaceResult->reviews) ?:                    $placeResult->setReviews($this->populatePlaceReviews($providerPlaceResult->reviews));
        !isset($providerPlaceResult->events) ?:                     $placeResult->setEvents($this->populatePlaceEvents($providerPlaceResult->events));

        !isset($providerPlaceResult->geometry) ?: $placeResult->setGeometry(
            $this->populatePlaceGeometry(
                $providerPlaceResult->geometry,
                $placeResult->getGeometry()
            )
        );

        return $placeResult;
    }

    /**
     * @param  \stdClass   $providerPlaceEvents
     * @param  PlaceEvents $placeEvents
     * @return PlaceEvents
     */
    protected function populatePlaceEvents($providerPlaceEvents, PlaceEvents $placeEvents = null)
    {
        if (null === $placeEvents) {
            $placeEvents = new PlaceEvents();
        }

        foreach ($providerPlaceEvents as $providerPlaceEvent) {
            $placeEvents->add($this->populatePlaceEvent($providerPlaceEvent));
        }

        return $placeEvents;
    }

    /**
     * @param  \stdClass  $providerPlaceEvent
     * @param  PlaceEvent $placeEvent
     * @return PlaceEvent
     */
    protected function populatePlaceEvent($providerPlaceEvent, PlaceEvent $placeEvent = null)
    {
        if (null === $placeEvent) {
            $placeEvent = new PlaceEvent();
        }

        if (isset($providerPlaceEvent->event_id)) {
            $placeEvent->setId($providerPlaceEvent->event_id);
        }

        if (isset($providerPlaceEvent->summary)) {
            $placeEvent->setSummary($providerPlaceEvent->summary);
        }

        if (isset($providerPlaceEvent->url)) {
            $placeEvent->setUrl($providerPlaceEvent->url);
        }

        if (isset($providerPlaceEvent->start_time)) {
            $placeEvent->setStartTime(\DateTime::createFromFormat('U', $providerPlaceEvent->start_time));
        }

        return $placeEvent;
    }

    /**
     * @param  \stdClass    $providerPlaceReviews
     * @param  PlaceReviews $placeReviews
     * @return PlaceReviews
     */
    protected function populatePlaceReviews($providerPlaceReviews, PlaceReviews $placeReviews = null)
    {
        if (null === $placeReviews) {
            $placeReviews = new PlaceReviews();
        }

        foreach ($providerPlaceReviews as $providerPlaceReview) {
            $placeReviews->add($this->populatePlaceReview($providerPlaceReview));
        }

        return $placeReviews;
    }

    /**
     * @param  \stdClass   $providerPlaceReview
     * @param  PlaceReview $placeReview
     * @return PlaceReview
     */
    protected function populatePlaceReview($providerPlaceReview, PlaceReview $placeReview = null)
    {
        if (null === $placeReview) {
            $placeReview = new PlaceReview();
        }

        if (isset($providerPlaceReview->author_name)) {
            $placeReview->setAuthorName($providerPlaceReview->author_name);
        }

        if (isset($providerPlaceReview->author_url)) {
            $placeReview->setAuthorUrl($providerPlaceReview->author_url);
        }

        if (isset($providerPlaceReview->text)) {
            $placeReview->setText($providerPlaceReview->text);
        }

        if (isset($providerPlaceReview->time)) {
            $placeReview->setCreatedAt(\DateTime::createFromFormat('U', $providerPlaceReview->time));
        }

        if (isset($providerPlaceReview->aspects)) {
            $placeReview->setAspects($this->populatePlaceAspectRatings($providerPlaceReview->aspects));
        }

        return $placeReview;
    }

    /**
     * @param  \stdClass          $providerPlaceAspectRatings
     * @param  PlaceAspectRatings $placeAspectRatings
     * @return PlaceAspectRatings
     */
    protected function populatePlaceAspectRatings($providerPlaceAspectRatings, PlaceAspectRatings $placeAspectRatings = null)
    {
        if (null === $placeAspectRatings) {
            $placeAspectRatings = new PlaceAspectRatings();
        }

        foreach ($providerPlaceAspectRatings as $providerPlaceAspectRating) {
            $placeAspectRatings->add($this->populatePlaceAspectRating($providerPlaceAspectRating));
        }

        return $placeAspectRatings;
    }

    /**
     * @param  \stdClass         $providerPlaceAspectRating
     * @param  PlaceAspectRating $placeAspectRating
     * @return PlaceAspectRating
     */
    protected function populatePlaceAspectRating($providerPlaceAspectRating, PlaceAspectRating $placeAspectRating = null)
    {
        if (null === $placeAspectRating) {
            $placeAspectRating = new PlaceAspectRating();
        }

        if (isset($providerPlaceAspectRating->rating)) {
            $placeAspectRating->setRating(intval($providerPlaceAspectRating->rating));
        }

        if (isset($providerPlaceAspectRating->type)) {
            $placeAspectRating->setType($providerPlaceAspectRating->type);
        }

        return $placeAspectRating;
    }

    /**
     * @param  \stdClass                 $providerGeocoderAddressComponents
     * @param  GeocoderAddressComponents $geocoderAddressComponents
     * @return GeocoderAddressComponents
     */
    protected function populateGeocoderAddressComponents($providerGeocoderAddressComponents, GeocoderAddressComponents $geocoderAddressComponents = null)
    {
        if (null === $geocoderAddressComponents) {
            $geocoderAddressComponents = new GeocoderAddressComponents();
        }

        foreach ($providerGeocoderAddressComponents as $providerGeocoderAddressComponent) {
            $geocoderAddressComponents->add($this->populateGeocoderAddressComponent($providerGeocoderAddressComponent));
        }

        return $geocoderAddressComponents;
    }

    /**
     * @param  \stdClass                $providerGeocoderAddressComponent
     * @param  GeocoderAddressComponent $geocoderAddressComponent
     * @return GeocoderAddressComponent
     */
    protected function populateGeocoderAddressComponent($providerGeocoderAddressComponent, GeocoderAddressComponent $geocoderAddressComponent = null)
    {
        if (null === $geocoderAddressComponent) {
            $geocoderAddressComponent = new GeocoderAddressComponent();
        }

        if (isset($providerGeocoderAddressComponent->long_name)) {
            $geocoderAddressComponent->setLongName($providerGeocoderAddressComponent->long_name);
        }

        if (isset($providerGeocoderAddressComponent->short_name)) {
            $geocoderAddressComponent->setShortName($providerGeocoderAddressComponent->short_name);
        }

        if (isset($providerGeocoderAddressComponent->types)) {
            $geocoderAddressComponent->setTypes($providerGeocoderAddressComponent->types);
        }

        return $geocoderAddressComponent;
    }

    /**
     * @param  \stdClass     $providerPlaceGeometry
     * @param  PlaceGeometry $placeGeometry
     * @return PlaceGeometry
     */
    protected function populatePlaceGeometry($providerPlaceGeometry, PlaceGeometry $placeGeometry = null)
    {
        if (null === $placeGeometry) {
            $placeGeometry = new PlaceGeometry();
        }

        if (isset($providerPlaceGeometry->location)) {
            $placeGeometry->setLocation(
                $this->populateLatLng(
                    $providerPlaceGeometry->location,
                    $placeGeometry->getLocation()
                )
            );
        }

        if (isset($providerPlaceGeometry->viewport)) {
            $placeGeometry->setViewport(
                $this->populateLatLngBounds(
                    $providerPlaceGeometry->viewport,
                    $placeGeometry->getViewport()
                )
            );
        }

        return $placeGeometry;
    }
}
