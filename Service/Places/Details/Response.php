<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Details;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractResponse as BaseResponse,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceResult;

/**
 * {@inheritdoc}
 *
 * @see https://developers.google.com/places/documentation/search#PlaceSearchResults
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Response extends BaseResponse
{
    /**
     * @var PlaceResult
     */
    protected $place;

    /**
     * @param PlaceResult $place
     */
    public function setPlace(PlaceResult $place = null)
    {
        $this->place = $place;

        return $this;
    }

    /**
     * @return PlaceResult
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param  string   $status
     * @return Response
     */
    public function setStatus($status)
    {
        if (!in_array($status, Status::getStatuses())) {
            throw new \InvalidArgumentException(sprintf('You give "%s" and you have to provide one of these statuses : "%s"', $status, implode(',', Status::getStatuses())));
        }
        $this->status = $status;

        return $this;
    }
}
