<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Details;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractRequest;

/**
 * {@inheritdoc}
 *
 * @see https://developers.google.com/places/documentation/search?hl=fr#RadarSearchRequests
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Request extends AbstractRequest
{
    /**
     * @var string A textual identifier that uniquely identifies a place
     */
    protected $reference;

    /**
     * @param  string  $reference
     * @return Request
     */
    public function setReference($reference)
    {
        if (0 >= mb_strlen($reference)) {
            throw new \InvalidArgumentException('You have to provide a valid reference');
        } else {
            $this->reference = $reference;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isValid()
    {
        return
            parent::isValid()
            && null !== $this->reference;
        ;
    }
}
