<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Details;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractService,
    \Yvann\GoogleAPIBundle\Service\AbstractRequest,
    \Yvann\GoogleAPIBundle\Service\AbstractResponse;

/**
 * Once you have a reference from a Place Search, you can request more details about a particular establishment or point of interest by initiating a Place Details request. A Place Details request returns more comprehensive information about the indicated place such as its complete address, phone number, user rating and reviews.
 *
 * @see https://developers.google.com/places/documentation/details?hl=fr
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Service extends AbstractService
{
    /**
     * @return string
     */
    public static function getServiceId()
    {
        return 'places.details';
    }

    /**
     * @return \Yvann\GoogleAPIBundle\Service\AbstractResponse|Response
     */
    protected function getNewResponse()
    {
        return new Response();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function generateProviderRequestParameters(AbstractRequest $request)
    {
        return array_merge(parent::generateProviderRequestParameters($request), [
            'reference' => $request->getReference() ?: false,
        ]);
    }

    /**
     * @param AbstractRequest $request
     * @param $providerResponse
     * @param  AbstractResponse $response
     * @return Response
     */
    protected function populateResponse(AbstractRequest $request, $providerResponse, AbstractResponse $response)
    {
        $response
            ->setStatus($providerResponse->status)
            ->setHtmlAttributions(isset($providerResponse->html_attributions) ? $providerResponse->html_attributions : [])
            ->setPlace(isset($providerResponse->result) ? $this->populatePlaceResult($providerResponse->result) : null);

        return $response;
    }
}
