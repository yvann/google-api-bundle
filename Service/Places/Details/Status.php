<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Details;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractStatus as BaseStatus;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Status extends BaseStatus
{
    const NOT_FOUND = 'NOT_FOUND';

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public static function getStatuses()
    {
        return array_merge(parent::getStatuses(), [
            self::NOT_FOUND,
        ]);
    }
}
