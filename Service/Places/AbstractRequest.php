<?php

namespace Yvann\GoogleAPIBundle\Service\Places;

use \Yvann\GoogleAPIBundle\Service\AbstractRequest as BaseRequest;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractRequest extends BaseRequest
{

}
