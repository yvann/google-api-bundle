<?php

namespace Yvann\GoogleAPIBundle\Service\Places;

use \Yvann\GoogleAPIBundle\Service\AbstractStatus as BaseStatus;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractStatus extends BaseStatus
{
}
