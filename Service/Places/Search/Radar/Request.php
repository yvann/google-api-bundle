<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Radar;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractRequest;

/**
 * {@inheritdoc}
 *
 * @see https://developers.google.com/places/documentation/search?hl=fr#RadarSearchRequests
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Request extends AbstractRequest
{
    public function __construct($sensor, $key, $language = null)
    {
        $this->radius = 50000;

        parent::__construct($sensor, $key, $language);
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isValid()
    {
        return
            parent::isValid()
            && null !== $this->location && 0 < $this->radius && 50000 >= $this->radius
            && ($this->keyword || $this->name || $this->types)
        ;
    }
}
