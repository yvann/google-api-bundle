<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Radar;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractService,
    \Yvann\GoogleAPIBundle\Service\AbstractRequest,
    \Yvann\GoogleAPIBundle\Service\AbstractResponse;

/**
 * The Google Places API Radar Search Service allows you to search for up to 200 Places at once, but with less detail than is typically returned from a Text Search or Nearby Search request. With Radar Search, you can create applications that help users identify specific areas of interest within a geographic area. The search response will include up to 200 Places, identified only by their geographic coordinates and Place reference. You can send a Place Details request for more information about any of the Places in the response.
 *
 * @see https://developers.google.com/places/documentation/search?hl=fr#RadarSearchRequests
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Service extends AbstractService
{
    /**
     * @return string
     */
    public static function getServiceId()
    {
        return 'places.search.radar';
    }

    /**
     * @return \Yvann\GoogleAPIBundle\Service\AbstractResponse|Response
     */
    protected function getNewResponse()
    {
        return new Response();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function generateProviderRequestParameters(AbstractRequest $request)
    {
        return array_merge(parent::generateProviderRequestParameters($request), [
            'mincost' => $request->getPriceRange()->getMin() ?: false,
            'maxcost' => $request->getPriceRange()->getMax() ?: false,
        ]);
    }
}
