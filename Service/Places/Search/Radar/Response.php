<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Radar;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractResponse as BaseResponse;

/**
 * {@inheritdoc}
 *
 * @see https://developers.google.com/places/documentation/search#PlaceSearchResults
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Response extends BaseResponse
{
}
