<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Nearby;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractService,
    \Yvann\GoogleAPIBundle\Service\AbstractRequest,
    \Yvann\GoogleAPIBundle\Service\AbstractResponse,
    \Yvann\GoogleAPIBundle\GoogleAPIEvents,
    \Yvann\GoogleAPIBundle\Event\GoogleAPIEvent,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Request\RankBy;

/**
 * A Nearby Search lets you search for Places within a specified area.
 * You can refine your search request by supplying keywords or specifying the type of Place you are searching for.
 *
 * @see https://developers.google.com/places/documentation/search
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Service extends AbstractService
{
    /**
     * @return string
     */
    public static function getServiceId()
    {
        return 'places.search.nearby';
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            sprintf(GoogleAPIEvents::SERVICE_POST_EXECUTE, static::getServiceId()) => 'onServicePostExecute',
        );
    }

    /**
     * @param GoogleAPIEvent $event
     */
    public function onServicePostExecute(GoogleAPIEvent $event)
    {
        $service = $event->getService();
        $request = $event->getRequest();
        $response = $event->getResponse();

        while (Status::OK === $response->getStatus() && $response->hasNextPageToken() && $request->isExtended()) {
            sleep(2);

            $request->setPageToken($response->getNextPageToken());
            $response = $service->doExecute($request, $response);
        }
    }

    /**
     * @return \Yvann\GoogleAPIBundle\Service\AbstractResponse|Response
     */
    protected function getNewResponse()
    {
        return new Response();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function generateProviderRequestParameters(AbstractRequest $request)
    {
        return array_merge(parent::generateProviderRequestParameters($request), [
            'rankby' => $request->getRankBy() ?: false,
            'pagetoken' => $request->getPageToken() ?: false,
            'minprice' => $request->getPriceRange()->getMin() ?: false,
            'maxprice' => $request->getPriceRange()->getMax() ?: false,
        ]);
    }
}
