<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Nearby;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractRequest,
    \Yvann\GoogleAPIBundle\Service\Places\Search\Nearby\Request\RankBy;

/**
 * A Nearby Search lets you search for Places within a specified area.
 * You can refine your search request by supplying keywords or specifying the type of Place you are searching for.
 *
 * @see https://developers.google.com/places/documentation/search?hl=fr#PlaceSearchRequests
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Request extends AbstractRequest
{
    /**
     * @var string Specifies the order in which results are listed.
     */
    protected $rankBy = RankBy::PROMINENCE;

    /**
     * @var string The token that can be used to return up to 20 additional results
     * @see https://developers.google.com/places/documentation/#PlaceSearchPaging
     */
    protected $pageToken;

    /**
     * @var bool Does the service must be called multiple times if there are more results
     * @see https://developers.google.com/places/documentation/#PlaceSearchPaging
     */
    protected $extended;

    public function __construct($sensor, $key, $language = null)
    {
        $this->extended = false;

        parent::__construct($sensor, $key, $language);
    }

    /**
     * @param  string  $rankBy
     * @return Request
     */
    public function setRankBy($rankBy)
    {
        if (in_array($rankBy, RankBy::getValues())) {
            $this->rankBy = $rankBy;
        } else {
            throw new \InvalidArgumentException('The place search rank by can only be : '.implode(', ',RankBy::getValues()));
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getRankBy()
    {
        return $this->rankBy;
    }

    /**
     * @param $pageToken
     * @return Request
     */
    public function setPageToken($pageToken)
    {
        $this->pageToken = $pageToken;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPageToken()
    {
        return $this->pageToken;
    }

    /**
     * @param bool $extended
     * @return Request
     */
    public function setExtended($extended)
    {
        $this->extended = (bool) $extended;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExtended()
    {
        return true === $this->extended;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isValid()
    {
        return
            parent::isValid()
            && null !== $this->location
            && (
                (0 < $this->radius && 50000 >= $this->radius && RankBy::PROMINENCE === $this->rankBy)
                || (null === $this->radius && RankBy::DISTANCE === $this->rankBy && ($this->keyword || $this->name || $this->types))
            )
        ;
    }
}
