<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Nearby;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractResponse as BaseResponse;

/**
 * A Nearby Search lets you search for Places within a specified area.
 * You can refine your search request by supplying keywords or specifying the type of Place you are searching for.
 *
 * @see https://developers.google.com/places/documentation/search#PlaceSearchResults
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Response extends BaseResponse
{
}
