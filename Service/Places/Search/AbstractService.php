<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractService as BaseService,
    \Yvann\GoogleAPIBundle\Service\AbstractRequest,
    \Yvann\GoogleAPIBundle\Service\AbstractResponse;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractService extends BaseService
{
    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function generateProviderRequestParameters(AbstractRequest $request)
    {
        return array_merge(parent::generateProviderRequestParameters($request), [
            'location' => sprintf(
                '%f,%f',
                $request->getLocation()->getLatitude(),
                $request->getLocation()->getLongitude()
            ),
            'radius' => $request->getRadius() ?: false,
            'keyword' => $request->getKeyword() ?: false,
            'name' => $request->getName() ?: false,
            'types' => $request->getTypes() ? implode('|', $request->getTypes()) : false,
            'opennow' => $request->getOpenNow() ? 'true' : false,
            'zagatselected' => $request->getZagatSelected() ? 'true' : false,
        ]);
    }

    /**
     * @param AbstractRequest $request
     * @param $providerResponse
     * @param  AbstractResponse $response
     * @return Response
     */
    protected function populateResponse(AbstractRequest $request, $providerResponse, AbstractResponse $response)
    {
        $response
            ->setStatus($providerResponse->status)
            ->setNextPageToken(isset($providerResponse->next_page_token) ? $providerResponse->next_page_token : null)
            ->setHtmlAttributions(isset($providerResponse->html_attributions) ? $providerResponse->html_attributions : []);

        foreach ($providerResponse->results as $providerPlaceResult) {
            $response->addPlace($this->populatePlaceResult($providerPlaceResult));
        }

        return $response;
    }
}
