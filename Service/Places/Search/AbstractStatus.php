<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractStatus as BaseStatus;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractStatus extends BaseStatus
{

}
