<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Text;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractRequest;

/**
 * {@inheritdoc}
 *
 * @see https://developers.google.com/places/documentation/search?hl=fr#TextSearchRequests
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Request extends AbstractRequest
{
    /**
     * @var string  The text string on which to search, for example: "restaurant". The Place service will return candidate matches based on this string and order the results based on their perceived relevance.
     */
    protected $query;

    /**
     * @param $keyword
     * @return void
     * @throws \BadMethodCallException
     */
    public function setKeyword($keyword)
    {
        throw new \BadMethodCallException('Text search does not use "keyword" parameter');
    }

    /**
     * @param $name
     * @return void
     * @throws \BadMethodCallException
     */
    public function setName($name)
    {
        throw new \BadMethodCallException('Text search does not use "name" parameter');
    }

    /**
     * @param string $query
     */
    public function setQuery($query)
    {
        if (0 === mb_strlen($query)) {
            throw new \InvalidArgumentException('You have to provide a query of at least 1 character');
        } else {
            $this->query = $query;
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * {@inheritdoc}
     *
     * @return bool
     */
    public function isValid()
    {
        return
            parent::isValid()
            && null !== $this->query
        ;
    }
}
