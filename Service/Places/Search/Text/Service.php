<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Text;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractService,
    \Yvann\GoogleAPIBundle\Service\AbstractRequest,
    \Yvann\GoogleAPIBundle\Service\AbstractResponse;

/**
 * The Google Places API Text Search Service is a web service that returns information about a set of Places based on a string — for example "pizza in New York" or "shoe stores near Ottawa". The service responds with a list of Places matching the text string and any location bias that has been set. The search response will include a list of Places, you can send a Place Details request for more information about any of the Places in the response.
 *
 * @see https://developers.google.com/places/documentation/search?hl=fr#TextSearchRequests
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Service extends AbstractService
{
    /**
     * @return string
     */
    public static function getServiceId()
    {
        return 'places.search.text';
    }

    /**
     * @return \Yvann\GoogleAPIBundle\Service\AbstractResponse|Response
     */
    protected function getNewResponse()
    {
        return new Response();
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    protected function generateProviderRequestParameters(AbstractRequest $request)
    {
        return array_merge(parent::generateProviderRequestParameters($request), [
            'query' => $request->getQuery() ?: false,
            'mincost' => $request->getPriceRange()->getMin() ?: false,
            'maxcost' => $request->getPriceRange()->getMax() ?: false,
        ]);
    }
}
