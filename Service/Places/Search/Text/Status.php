<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search\Text;

use \Yvann\GoogleAPIBundle\Service\Places\Search\AbstractStatus as BaseStatus;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class Status extends BaseStatus
{

}
