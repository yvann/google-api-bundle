<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractRequest as BaseRequest,
    \Yvann\GoogleAPIBundle\Model\Base\LatLngInterface,
    \Yvann\GoogleAPIBundle\Model\Places\PlacePriceLevel,
    \Yvann\GoogleAPIBundle\Model\Places\PlacePriceRange;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractRequest extends BaseRequest
{
    /**
     * @var LatLngInterface The latitude/longitude around which to retrieve Place information
     */
    protected $location;

    /**
     * @var int Defines the distance (in meters) within which to return Place results
     */
    protected $radius;

    /**
     * @var string A term to be matched against all available fields, including but not limited to name, type, and address, as well as customer reviews and other third-party content.
     */
    protected $keyword;

    /**
     * @var string A term to be matched against the names of Places. Results will be restricted to those containing the passed name value.
     */
    protected $name;

    /**
     * @var array Restricts the results to Places matching at least one of the specified types.
     * @see https://developers.google.com/maps/documentation/places/supported_types
     */
    protected $types;

    /**
     * @var PlacePriceRange
     */
    protected $priceRange;

    /**
     * @var bool Returns only those Places that are open for business at the time the query is sent. Places that do not specify opening hours in the Google Places database will not be returned if you include this parameter in your query.
     */
    protected $openNow;

    /**
     * @var bool Restrict your search to only those locations that are Zagat selected businesses.
     */
    protected $zagatSelected;

    public function __construct($sensor, $key, $language = null)
    {
        $this->priceRange = new PlacePriceRange();

        parent::__construct($sensor, $key, $language);
    }

    /**
     * @param LatLngInterface $location
     *
     * @return Request
     */
    public function setLocation(LatLngInterface $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return LatLngInterface
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param  int     $radius
     * @return Request
     */
    public function setRadius($radius)
    {
        if (is_int($radius) && 0 <= $radius) {
            $this->radius = $radius;
        } else {
            throw new \InvalidArgumentException('The radius must be a positive integer');
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @return null|string
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param $keyword
     * @return Request
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return Request
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param  array   $types
     * @return Request
     */
    public function setTypes(array $types)
    {
        $this->types = array();
        $this->addTypes($types);

        return $this;
    }

    /**
     * @param  array   $types
     * @return Request
     */
    public function addTypes(array $types)
    {
        foreach ($types as $type) {
            $this->addType($type);
        }

        return $this;
    }

    /**
     * @param $type
     * @return Request
     */
    public function addType($type)
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * @param PlacePriceRange $priceRange
     */
    public function setPriceRange(PlacePriceRange $priceRange)
    {
        $this->priceRange = $priceRange;

        return $this;
    }

    /**
     * @return PlacePriceRange
     */
    public function getPriceRange()
    {
        return $this->priceRange;
    }

    /**
     * @param  boolean $openNow
     * @return Request
     */
    public function setOpenNow($openNow)
    {
        $this->openNow = (bool) $openNow;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getOpenNow()
    {
        return $this->openNow;
    }

    /**
     * @param  boolean $zagatSelected
     * @return Request
     */
    public function setZagatSelected($zagatSelected)
    {
        $this->zagatSelected = (bool) $zagatSelected;

        return $this;
    }

    /**
     * @return boolean
     */
    public function getZagatSelected()
    {
        return $this->zagatSelected;
    }
}
