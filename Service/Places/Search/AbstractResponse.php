<?php

namespace Yvann\GoogleAPIBundle\Service\Places\Search;

use \Yvann\GoogleAPIBundle\Service\Places\AbstractResponse as BaseResponse,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceResults,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceResult;

/**
 * {@inheritdoc}
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractResponse extends BaseResponse
{
    /**
     * @var PlaceResults
     */
    protected $places;

    /**
     * @var string Next page token
     */
    protected $nextPageToken;

    public function __construct()
    {
        $this->places = new PlaceResults();
        $this->htmlAttributions = [];
    }

    /**
     * @return PlaceResults
     */
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * @param  PlaceResult      $placeResult
     * @return AbstractResponse
     */
    public function addPlace(PlaceResult $placeResult)
    {
        $this->places->add($placeResult);

        return $this;
    }

    /**
     * Does the next page token exists
     *
     * @return bool
     */
    public function hasNextPageToken()
    {
        return null !== $this->nextPageToken;
    }

    /**
     * Gets the next page token
     *
     * @return string
     */
    public function getNextPageToken()
    {
        return $this->nextPageToken;
    }

    /**
     * Sets the next page token
     *
     * @param  string           $nextPageToken
     * @return AbstractResponse
     */
    public function setNextPageToken($nextPageToken)
    {
        $this->nextPageToken = $nextPageToken;

        return $this;
    }
}
