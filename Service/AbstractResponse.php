<?php

namespace Yvann\GoogleAPIBundle\Service;

/**
 * Abstract response class for accesing Google API
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractResponse
{
    /**
     * @var string The status of the response
     */
    protected $status;

    /**
     * @var string A message to explain status if it is needed
     */
    protected $statusMessage;

    /**
     * Returns an object that represent the response
     *
     * @param string $status
     */
    public function __construct($status = null)
    {
        !$status ?: $this->setStatus($status);
    }

    /**
     * @param  string   $status
     * @return Response
     */
    public function setStatus($status)
    {
        if (!in_array($status, AbstractStatus::getStatuses())) {
            throw new \InvalidArgumentException(sprintf('You have to provide one of these statuses : "%s"', implode(',', AbstractStatus::getStatuses())));
        }
        $this->status = $status;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $statusMessage
     */
    public function setStatusMessage($statusMessage)
    {
        if ($statusMessage instanceof \Exception) {
            $statusMessage = sprintf('%s (%s) : %s', $statusMessage->getFile(), $statusMessage->getLine(), $statusMessage->getMessage());
        }
        $this->statusMessage = $statusMessage;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatusMessage()
    {
        return $this->statusMessage;
    }
}
