<?php

namespace Yvann\GoogleAPIBundle\Service;

/**
 * Status of the request
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
abstract class AbstractStatus
{
    const OK = 'OK';
    const ZERO_RESULTS = 'ZERO_RESULTS';
    const OVER_QUERY_LIMIT = 'OVER_QUERY_LIMIT';
    const REQUEST_DENIED = 'REQUEST_DENIED';
    const INVALID_REQUEST = 'INVALID_REQUEST';
    const UNKNOWN_ERROR = 'UNKNOWN_ERROR';

    /**
     * Disabled constructor
     */
    final public function __construct()
    {
        throw new \Exception(sprintf('The class "%s" can not be instanciate.', get_class($this)));
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            self::OK,
            self::ZERO_RESULTS,
            self::OVER_QUERY_LIMIT,
            self::REQUEST_DENIED,
            self::INVALID_REQUEST,
            self::UNKNOWN_ERROR,
        ];
    }
}
