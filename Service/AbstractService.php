<?php

namespace Yvann\GoogleAPIBundle\Service;

use \Symfony\Component\EventDispatcher\EventDispatcher,
    \Symfony\Component\EventDispatcher\EventSubscriberInterface;

use \Buzz\Browser as HttpClient;

use \Yvann\GoogleAPIBundle\Event\GoogleAPIEvent,
    \Yvann\GoogleAPIBundle\GoogleAPIEvents;

use \Yvann\GoogleAPIBundle\Model\Base\LatLng,
    \Yvann\GoogleAPIBundle\Model\Base\LatLngBounds;

/**
 * Abstract service class for accesing Google API
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractService implements EventSubscriberInterface
{
    /**
     * Number of retries in case of transaction failure
     */
    const PROVIDER_TRANSACTION_MAX_RETRIES = 1;

    /**
     * @var EventDispatcher Event dispatcher to deal with events
     */
    protected $eventDispatcher;

    /**
     * @var HttpClient HTTP Client
     */
    protected $httpClient;

    /**
     * @var string Base URL of service
     */
    protected $url;

    /**
     * @param EventDispatcher $eventDispatcher Event dispatcher to deal with events
     * @param HttpClient      $httpClient      HTTP Client
     * @param string          $url             Base URL of service
     */
    public function __construct(EventDispatcher $eventDispatcher, HttpClient $httpClient, $url)
    {
        $this
            ->setEventDispatcher($eventDispatcher)
            ->setHttpClient($httpClient)
            ->setUrl($url);

        $this->eventDispatcher->addSubscriber($this);

        $this->dispatch(GoogleAPIEvents::SERVICE_POST_CONSTRUCT, new GoogleAPIEvent($this));
    }

    /**
     * @return string
     */
    public static function getServiceId()
    {
        throw new \RuntimeException('You have to provide a service id in your service');
    }

    /**
     * @return string
     */
    public static function getCommonServiceId()
    {
        return 'all';
    }

    /**
     * Eases the event dispatching
     *
     * @param string         $eventName
     * @param GoogleAPIEvent $event
     */
    public function dispatch($eventName, GoogleAPIEvent $event)
    {
        $this->eventDispatcher->dispatch(sprintf($eventName, static::getCommonServiceId()), $event);
        $this->eventDispatcher->dispatch(sprintf($eventName, static::getServiceId()), $event);
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array();
    }

    /**
     * @param EventDispatcher $eventDispatcher
     *
     * @return AbstractService
     */
    public function setEventDispatcher(EventDispatcher $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;

        return $this;
    }

    /**
     * @return EventDispatcher
     */
    public function getEventDispatcher()
    {
        return $this->eventDispatcher;
    }

    /**
     * @param HttpClient $httpClient
     *
     * @return AbstractService
     */
    public function setHttpClient(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;

        return $this;
    }

    /**
     * @return HttpClient
     */
    public function getHttpClient()
    {
        return $this->httpClient;
    }

    /**
     * @param string $url
     *
     * @return AbstractService
     */
    public function setUrl($url)
    {
        if (is_string($url) && 0 < mb_strlen($url)) {
            $this->url = $url;
        } else {
            throw new \InvalidArgumentException(sprintf('URL must be a valid URL : "%s" given', $url));
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @return AbstractResponse
     */
    abstract protected function getNewResponse();

    /**
     * @param  AbstractRequest         $request
     * @return AbstractResponse
     * @throws \BadMethodCallException
     */
    public function execute(AbstractRequest $request)
    {
        $this->dispatch(GoogleAPIEvents::SERVICE_PRE_EXECUTE, new GoogleAPIEvent($this, $request));

        if (!$request->isValid()) {
            throw new \BadMethodCallException('Your request does not provide the requirements to be executed');
        }

        $response = $this->doExecute($request, $this->getNewResponse());

        $this->dispatch(GoogleAPIEvents::SERVICE_POST_EXECUTE, new GoogleAPIEvent($this, $request, null, null, null, $response));

        return $response;
    }

    /**
     * @param  AbstractRequest  $request
     * @param  AbstractResponse $response
     * @return AbstractResponse
     */
    public function doExecute(AbstractRequest $request, AbstractResponse $response)
    {
        $this->dispatch(GoogleAPIEvents::SERVICE_PRE_PROVIDER_REQUEST_GENERATION, new GoogleAPIEvent($this, $request));

        $providerRequest = $this->generateProviderRequest($request);

        $this->dispatch(GoogleAPIEvents::SERVICE_POST_PROVIDER_REQUEST_GENERATION, new GoogleAPIEvent($this, $request, null, $providerRequest));

        $retries = 0;
        $caughtException = false;
        do {
            try {
                $providerResponse = $this->generateProviderResponse($request, $providerRequest, $retries);
                $caughtException = false;
            } catch (\Exception $e) {
                $providerResponse = false;
                $caughtException = $e;
            }
        } while (false === $providerResponse && self::PROVIDER_TRANSACTION_MAX_RETRIES > $retries++);

        if ($caughtException) {
            return $response
                ->setStatus(AbstractStatus::UNKNOWN_ERROR)
                ->setStatusMessage($caughtException);
        } else {
            $response
                ->setStatus($providerResponse->status);
        }

        return $this->populateResponse($request, $providerResponse, $response);
    }

    /**
     * Generates the URL
     *
     * @return string
     */
    protected function generateProviderRequest(AbstractRequest $request)
    {
        $this->dispatch(GoogleAPIEvents::SERVICE_PRE_PROVIDER_REQUEST_QUERY_GENERATION, new GoogleAPIEvent($this, $request));

        $providerRequestParameters = $this->generateProviderRequestParameters($request);

        $this->dispatch(GoogleAPIEvents::SERVICE_POST_PROVIDER_REQUEST_QUERY_GENERATION, new GoogleAPIEvent($this, $request, $providerRequestParameters));

        return sprintf(
            '%s/json?%s',
            rtrim($this->getUrl()),
            http_build_query(array_filter($providerRequestParameters, function($parameterKey) {
                return false !== $parameterKey;
            }))
        );
    }

    /**
     * Generates the URL query parameters
     *
     * @return array
     */
    protected function generateProviderRequestParameters(AbstractRequest $request)
    {
        return [
            'key' => $request->getKey(),
            'sensor' => $request->hasSensor() ? 'true' : 'false',
            'language' => $request->getLanguage(),
        ];
    }

    /**
     * Generates provider response
     *
     * @param AbstractRequest $request
     * @param string          $providerRequest
     * @param int             $retries
     *
     * @return \stdClass
     */
    protected function generateProviderResponse(AbstractRequest $request, $providerRequest, $retries = 0)
    {
        $this->dispatch(GoogleAPIEvents::SERVICE_PRE_TRANSACTION, new GoogleAPIEvent($this, $request, null, $providerRequest));

        $providerTransactionResponse = $this->httpClient->get($providerRequest);
        if ($providerTransactionResponse->isOK()) {
            $providerResponse = json_decode($providerTransactionResponse->getContent(), false);
        } else {
            throw new \RuntimeException(sprintf(
                'An error occurred during provider transaction : %d status code - %s',
                $providerTransactionResponse->getStatusCode(),
                $providerTransactionResponse->getReasonPhrase()
            ));
        }

        $this->dispatch(GoogleAPIEvents::SERVICE_POST_TRANSACTION, new GoogleAPIEvent($this, $request, null, $providerRequest, $providerResponse));

        return $providerResponse;
    }

    /**
     * @param AbstractRequest $request
     * @param $providerResponse
     * @param  AbstractResponse $response
     * @return AbstractResponse
     */
    abstract protected function populateResponse(AbstractRequest $request, $providerResponse, AbstractResponse $response);

    /**
     * @param  \stdClass $providerLatLng
     * @param  LatLng    $latLng
     * @return LatLng
     */
    protected function populateLatLng($providerLatLng, LatLng $latLng = null)
    {
        if (null === $latLng) {
            $latLng = new LatLng();
        }

        if (
            isset($providerLatLng->lat)
            && isset($providerLatLng->lng)
        ) {
            $latLng
                ->setLatitude($providerLatLng->lat)
                ->setLongitude($providerLatLng->lng);
        }

        return $latLng;
    }

    /**
     * @param  \stdClass    $providerLatLngBounds
     * @param  LatLngBounds $latLngBounds
     * @return LatLngBounds
     */
    protected function populateLatLngBounds($providerLatLngBounds, LatLngBounds $latLngBounds = null)
    {
        if (null === $latLngBounds) {
            $latLngBounds = new LatLngBounds();
        }

        if (
            isset($providerLatLngBounds->southwest)
            && isset($providerLatLngBounds->northeast)
        ) {
            $latLngBounds
                ->setSouthWest(
                    $this->populateLatLng(
                        $providerLatLngBounds->southwest,
                        $latLngBounds->getSouthWest()
                    )
                )
                ->setNorthEast(
                    $this->populateLatLng(
                        $providerLatLngBounds->northeast,
                        $latLngBounds->getNorthEast()
                    )
                );
        }

        return $latLngBounds;
    }
}
