<?php

namespace Yvann\GoogleAPIBundle\Service;

/**
 * Abstract request class for accessing Google API
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
abstract class AbstractRequest
{
    /**
     * @var boolean  Indicates whether or not the request came from a device using a location sensor
     */
    protected $sensor;

    /**
     * @var string This key identifies your application for purposes of quota management
     */
    protected $key;

    /**
     * @var string The language code, indicating in which language the results should be returned, if possible
     */
    protected $language;

    public function __construct($sensor, $key, $language = null)
    {
        $this
            ->setSensor($sensor)
            ->setKey($key);

        !$language ?: $this->setLanguage($language);
    }

    /**
     * @param boolean $sensor
     *
     * @return AbstractRequest
     */
    public function setSensor($sensor)
    {
        if (is_bool($sensor)) {
            $this->sensor = $sensor;
        } else {
            throw new \InvalidArgumentException(sprintf('Sensor must be true or false : %s provided', $sensor));
        }

        return $this;
    }

    /**
     * @return boolean
     */
    public function hasSensor()
    {
        return $this->sensor;
    }

    /**
     * @param string $key
     *
     * @return AbstractRequest
     */
    public function setKey($key)
    {
        if (!empty($key)) {
            $this->key = $key;
        } else {
            throw new \InvalidArgumentException('Key must be a non empty string');
        }

        return $this;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @param string $language
     *
     * @return AbstractRequest
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Determines whether the request is valid before execution or not
     *
     * @return boolean
     */
    public function isValid()
    {
        return
            null !== $this->key
            && null !== $this->sensor
            && null !== $this->language
        ;
    }
}
