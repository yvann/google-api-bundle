<?php

namespace Yvann\GoogleAPIBundle\DependencyInjection;

use \Symfony\Component\Config\Definition\Builder\TreeBuilder,
    \Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('yvann_google_api');

        $rootNode
            ->append($this->addCommonNode())
            ->append($this->addServicesNode());

        return $treeBuilder;
    }

    public function addCommonNode()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('common');

        return $rootNode
            ->children()
                ->scalarNode('key')->isRequired()->cannotBeEmpty()->end()
                ->booleanNode('sensor')->cannotBeEmpty()->defaultFalse()->end()
                ->enumNode('language')->values(array(
                    'en',
                    'fr',
                ))->cannotBeEmpty()->defaultValue('en')->end()
            ->end()
        ;
    }

    public function addServicesNode()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('services');

        return $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->append($this->addServicesPlacesNode())
            ->end()
        ;
    }

    public function addServicesPlacesNode()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('places');

        return $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->append($this->addServicesPlacesSearchNode())
                ->arrayNode('details')
                ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('url')->cannotBeEmpty()->defaultValue('https://maps.googleapis.com/maps/api/place/details')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    public function addServicesPlacesSearchNode()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('search');

        return $rootNode
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('nearby')
                ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('url')->cannotBeEmpty()->defaultValue('https://maps.googleapis.com/maps/api/place/nearbysearch')->end()
                    ->end()
                ->end()
                ->arrayNode('radar')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('url')->cannotBeEmpty()->defaultValue('https://maps.googleapis.com/maps/api/place/radarsearch')->end()
                    ->end()
                ->end()
                ->arrayNode('text')
                ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('url')->cannotBeEmpty()->defaultValue('https://maps.googleapis.com/maps/api/place/textsearch')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
