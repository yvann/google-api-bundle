<?php

namespace Yvann\GoogleAPIBundle\DependencyInjection;

use \Symfony\Component\DependencyInjection\ContainerBuilder,
    \Symfony\Component\Config\FileLocator,
    \Symfony\Component\HttpKernel\DependencyInjection\Extension,
    \Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class YvannGoogleAPIExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\XmlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.xml');

        $this->loadCommonConfiguration($config['common'], $container);
        $this->loadServicesConfiguration($config['services'], $container);
    }

    /**
     * @param array            $config
     * @param ContainerBuilder $container
     */
    public function loadCommonConfiguration(array $config, ContainerBuilder $container)
    {
        foreach (array('key', 'sensor', 'language') as $parameterKey) {
            $container->setParameter(sprintf('yvann_google_api.%s', $parameterKey), $config[$parameterKey]);
        }
    }

    /**
     * @param array            $config
     * @param ContainerBuilder $container
     */
    public function loadServicesConfiguration(array $config, ContainerBuilder $container)
    {
        $this->loadServicesPlacesConfiguration($config['places'], $container);
    }

    /**
     * @param array            $config
     * @param ContainerBuilder $container
     */
    public function loadServicesPlacesConfiguration(array $config, ContainerBuilder $container)
    {
        $this->loadServicesPlacesSearchConfiguration($config['search'], $container);

        $container->setParameter('yvann_google_api.service.places.details.url', $config['details']['url']);
    }

    /**
     * @param array            $config
     * @param ContainerBuilder $container
     */
    public function loadServicesPlacesSearchConfiguration(array $config, ContainerBuilder $container)
    {
        $container->setParameter('yvann_google_api.service.places.search.nearby.url', $config['nearby']['url']);
        $container->setParameter('yvann_google_api.service.places.search.radar.url', $config['radar']['url']);
        $container->setParameter('yvann_google_api.service.places.search.text.url', $config['text']['url']);
    }
}
