<?php

namespace Yvann\GoogleAPIBundle;

/**
 * Represents all the events thrown by this bundle
 *
 * @author Yvann Boucher <yvann.boucher@leguide.com>
 */
final class GoogleAPIEvents
{
    /**
     * The yvann_google_api.service.post_construct event is thrown when the service is created
     *
     * @var string
     */
    const SERVICE_POST_CONSTRUCT = 'yvann_google_api.service.%s.post_construct';

    /**
     * The yvann_google_api.service.pre_execute event is thrown when the service prepares the execution of a request
     *
     * @var string
     */
    const SERVICE_PRE_EXECUTE = 'yvann_google_api.service.%s.pre_execute';

    /**
     * The yvann_google_api.service.post_execute event is thrown when the service has executed the request and prepares its return
     *
     * @var string
     */
    const SERVICE_POST_EXECUTE = 'yvann_google_api.service.%s.post_execute';

    /**
     * The yvann_google_api.service.pre_transaction event is thrown when the service prepares the transaction with provider
     *
     * @var string
     */
    const SERVICE_PRE_TRANSACTION = 'yvann_google_api.service.%s.pre_transaction';

    /**
     * The yvann_google_api.service.post_transaction event is thrown when the service has executed the transaction with provider
     *
     * @var string
     */
    const SERVICE_POST_TRANSACTION = 'yvann_google_api.service.%s.post_transaction';

    /**
     * The yvann_google_api.service.pre_provider_request_generation event is thrown when the service prepares the provider request
     *
     * @var string
     */
    const SERVICE_PRE_PROVIDER_REQUEST_GENERATION = 'yvann_google_api.service.%s.pre_provider_request_generation';

    /**
     * The yvann_google_api.service.post_provider_request_generation event is thrown when the service has generated the provider request
     *
     * @var string
     */
    const SERVICE_POST_PROVIDER_REQUEST_GENERATION = 'yvann_google_api.service.%s.post_provider_request_generation';

    /**
     * The yvann_google_api.service.pre_provider_request_query_generation event is thrown when the service prepares the provider request query parameters
     *
     * @var string
     */
    const SERVICE_PRE_PROVIDER_REQUEST_QUERY_GENERATION = 'yvann_google_api.service.%s.pre_provider_request_query_generation';

    /**
     * The yvann_google_api.service.post_provider_request_query_generation event is thrown when the service has generated the provider request query parameters
     *
     * @var string
     */
    const SERVICE_POST_PROVIDER_REQUEST_QUERY_GENERATION = 'yvann_google_api.service.%s.post_provider_request_query_generation';
}
