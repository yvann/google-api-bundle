<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

use \PhpCollection\Sequence;

/**
 * Defines a collection of PlaceEvent
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceEvent
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceEvents extends Sequence
{
}
