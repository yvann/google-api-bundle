<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

/**
 * Represents a single event of a place.
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceEvent
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceEvent
{
    /**
     * @var string The unique ID of this event.
     */
    protected $id;

    /**
     * @var string A textual description of the event. This property contains a string.
     */
    protected $summary;

    /**
     * @var string A URL pointing to details about the event.
     */
    protected $url;

    /**
     * @var \Datetime The event's start time
     */
    protected $startTime;

    /**
     * @param  string     $id
     * @return PlaceEvent
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  string     $summary
     * @return PlaceEvent
     */
    public function setSummary($summary)
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return string
     */
    public function getSummary()
    {
        return $this->summary;
    }

    /**
     * @param  string     $url
     * @return PlaceEvent
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param  \Datetime  $startTime
     * @return PlaceEvent
     */
    public function setStartTime(\Datetime $startTime)
    {
        $this->startTime = $startTime;

        return $this;
    }

    /**
     * @return \Datetime
     */
    public function getStartTime()
    {
        return $this->startTime;
    }
}
