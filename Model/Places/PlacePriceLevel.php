<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

/**
 * The price level of the Place, on a scale of 0 to 4
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlacePriceLevel
{
    const FREE = 0;
    const INEXPENSIVE = 1;
    const MODERATE = 2;
    const EXPENSIVE = 3;
    const VERY_EXPENSIVE = 4;

    /**
     * Disabled constructor
     */
    final public function __construct()
    {
        throw new \Exception(sprintf('The class "%s" can not be instanciate.', get_class($this)));
    }

    /**
     * {@inheritdoc}
     *
     * @return array
     */
    public static function getLevels()
    {
        return [
            self::FREE,
            self::INEXPENSIVE,
            self::MODERATE,
            self::EXPENSIVE,
            self::VERY_EXPENSIVE,
        ];
    }
}
