<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

use \PhpCollection\Sequence;

/**
 * Defines a collection of PlaceReview
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceReview
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceReviews extends Sequence
{
}
