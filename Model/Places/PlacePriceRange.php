<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

/**
 * The price level of the Place, on a scale of 0 to 4
 *
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlacePriceRange
{
    /**
     * @var int
     */
    protected $min;

    /**
     * @var int
     */
    protected $max;

    public function __construct($min = null, $max = null)
    {
        null === $min ?: $this->setMin($min);
        null === $max ?: $this->setMax($max);
    }

    /**
     * @param  int             $min
     * @return PlacePriceRange
     */
    public function setMin($min)
    {
        $min = intval($min);

        if (!in_array($min, PlacePriceLevel::getLevels())) {
            throw new \InvalidArgumentException(sprintf('You have to provide one of these levels : "%s"', implode(',', PlacePriceLevel::getLevels())));
        } elseif (null !== $this->max && $min > $this->max) {
            throw new \InvalidArgumentException('Min price level must be less than or equal to max price level');
        } else {
            $this->min = $min;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * @param  int             $max
     * @return PlacePriceRange
     */
    public function setMax($max)
    {
        if (!in_array($max, PlacePriceLevel::getLevels())) {
            throw new \InvalidArgumentException(sprintf('You have to provide one of these levels : "%s"', implode(',', PlacePriceLevel::getLevels())));
        } elseif (null !== $this->min && $max < $this->min) {
            throw new \InvalidArgumentException('Min price level must be less than or equal to max price level');
        } else {
            $this->max = intval($max);
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getMax()
    {
        return $this->max;
    }
}
