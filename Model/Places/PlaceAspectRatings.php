<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

use \PhpCollection\Sequence;

/**
 * Defines a collection of PlaceAspectRating
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceAspectRating
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceAspectRatings extends Sequence
{
}
