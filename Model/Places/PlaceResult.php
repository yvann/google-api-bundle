<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

use \Yvann\GoogleAPIBundle\Model\Geocoder\GeocoderAddressComponents,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceReviews,
    \Yvann\GoogleAPIBundle\Model\Places\PlaceEvents;

/**
 * Defines information about a Place.
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceResult
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceResult
{
    /**
     * @var string A unique identifier denoting this Place.
     */
    protected $id;

    /**
     * @var string An opaque string that may be used to retrieve up-to-date information about this Place.
     */
    protected $reference;

    /**
     * @var string URL to an image resource that can be used to represent this Place's category.
     */
    protected $icon;

    /**
     * @var string The Place's name
     */
    protected $name;

    /**
     * @var PlaceGeometry The Place's geometry-related information.
     */
    protected $geometry;

    /**
     * @var array An array of types for this Place.
     * @see https://developers.google.com/maps/documentation/places/supported_types
     */
    protected $types;

    /**
     * @var GeocoderAddressComponents a collection of separate address components used to compose a given address. For example, the address "111 8th Avenue, New York, NY" contains separate address components for "111" (the street number, "8th Avenue" (the route), "New York" (the city) and "NY" (the US state).
     */
    protected $addressComponents;

    /**
     * @var string A fragment of the Place's address for disambiguation (usually street name and locality).
     */
    protected $vicinity;

    /**
     * @var string
     */
    protected $formattedAddress;

    /**
     * @var string
     */
    protected $formattedPhoneNumber;

    /**
     * @var string
     */
    protected $internationalPhoneNumber;

    /**
     * @var float contains the Place's rating, from 0.0 to 5.0, based on user reviews.
     */
    protected $rating;

    /**
     * @var PlaceReviews A list of reviews of this Place.
     */
    protected $reviews;

    /**
     * @var PlaceEvents A list of events of this Place.
     */
    protected $events;

    public function __construct()
    {
        $this->types = [];
        $this->addressComponents = new GeocoderAddressComponents();
        $this->reviews = new PlaceReviews();
        $this->events = new PlaceEvents();
    }

    /**
     * @param  string      $id
     * @return PlaceResult
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param  string      $reference
     * @return PlaceResult
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return string
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param $icon
     * @return PlaceResult
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param  string      $name
     * @return PlaceResult
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param  PlaceGeometry $geometry
     * @return PlaceResult
     */
    public function setGeometry(PlaceGeometry $geometry)
    {
        $this->geometry = $geometry;

        return $this;
    }

    /**
     * @return PlaceGeometry
     */
    public function getGeometry()
    {
        return $this->geometry;
    }

    /**
     * @param  array       $types
     * @return PlaceResult
     */
    public function setTypes(array $types)
    {
        $this->types = array();
        foreach ($types as $type) {
            $this->addType($type);
        }

        return $this;
    }

    /**
     * @param $type
     * @return PlaceResult
     */
    public function addType($type)
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return bool
     */
    public function hasType($type)
    {
        return in_array($type, $this->types);
    }

    /**
     * @param  GeocoderAddressComponents $addressComponents
     * @return PlaceResult
     */
    public function setAddressComponents(GeocoderAddressComponents $addressComponents)
    {
        $this->addressComponents = $addressComponents;

        return $this;
    }

    /**
     * @return GeocoderAddressComponents
     */
    public function getAddressComponents()
    {
        return $this->addressComponents;
    }

    /**
     * @param $vicinity
     * @return PlaceResult
     */
    public function setVicinity($vicinity)
    {
        $this->vicinity = $vicinity;

        return $this;
    }

    /**
     * @return string
     */
    public function getVicinity()
    {
        return $this->vicinity;
    }

    /**
     * @param  string      $formattedAddress
     * @return PlaceResult
     */
    public function setFormattedAddress($formattedAddress)
    {
        $this->formattedAddress = $formattedAddress;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormattedAddress()
    {
        return $this->formattedAddress;
    }

    /**
     * @param  string      $formattedPhoneNumber
     * @return PlaceResult
     */
    public function setFormattedPhoneNumber($formattedPhoneNumber)
    {
        $this->formattedPhoneNumber = $formattedPhoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormattedPhoneNumber()
    {
        return $this->formattedPhoneNumber;
    }

    /**
     * @param  string      $internationalPhoneNumber
     * @return PlaceResult
     */
    public function setInternationalPhoneNumber($internationalPhoneNumber)
    {
        $this->internationalPhoneNumber = $internationalPhoneNumber;

        return $this;
    }

    /**
     * @return string
     */
    public function getInternationalPhoneNumber()
    {
        return $this->internationalPhoneNumber;
    }

    /**
     * @param float $rating
     * @return $this
     */
    public function setRating($rating)
    {
        $this->rating = floatval($rating);

        return $this;
    }

    /**
     * @return float
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param  PlaceReviews $placeReviews
     * @return PlaceResult
     */
    public function setReviews(PlaceReviews $reviews)
    {
        $this->reviews = $reviews;

        return $this;
    }

    /**
     * @return PlaceReviews
     */
    public function getReviews()
    {
        return $this->reviews;
    }

    /**
     * @param  PlaceEvents $placeEvents
     * @return PlaceResult
     */
    public function setEvents(PlaceEvents $events)
    {
        $this->events = $events;

        return $this;
    }

    /**
     * @return PlaceEvents
     */
    public function getEvents()
    {
        return $this->events;
    }
}
