<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

use \PhpCollection\Sequence;

/**
 * Defines a collection of PlaceResult
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceResult
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceResults extends Sequence
{
}
