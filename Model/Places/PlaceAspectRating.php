<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

/**
 * Defines information about an aspect of the place that users have reviewed.
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceAspectRating
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceAspectRating
{
    /**
     * @var int The rating of this aspect. For individual reviews this is an integer from 0 to 3. For aggregated ratings of a place this is an integer from 0 to 30.
     */
    protected $rating;

    /**
     * @var string The aspect type, e.g. "food", "decor", "service", "overall".
     */
    protected $type;

    /**
     * @param  int               $rating
     * @return PlaceAspectRating
     */
    public function setRating($rating)
    {
        if (!is_int($rating)) {
            throw new \InvalidArgumentException('The rating must be an integer');
        } else {
            $this->rating = $rating;
        }

        return $this;
    }

    /**
     * @return int
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param  string            $type
     * @return PlaceAspectRating
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
}
