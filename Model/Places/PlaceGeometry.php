<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

use \Yvann\GoogleAPIBundle\Model\Base\LatLng,
    \Yvann\GoogleAPIBundle\Model\Base\LatLngBounds;

/**
 * Defines information about the geometry of a Place.
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceGeometry
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceGeometry
{
    /**
     * @var LatLng The Place's position.
     */
    protected $location;

    /**
     * @var LatLngBounds The preferred viewport when displaying this Place on a map.
     */
    protected $viewport;

    /**
     * @param LatLng       $location
     * @param LatLngBounds $viewport
     */
    public function __construct(LatLng $location = null, LatLngBounds $viewport = null)
    {
        !$location ?: $this->setLocation($location);
        !$viewport ?: $this->setViewport($viewport);
    }

    /**
     * @param  LatLng        $location
     * @return Placegeometry
     */
    public function setLocation(LatLng $location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return LatLng
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param  LatLngBounds  $viewport
     * @return Placegeometry
     */
    public function setViewport(LatLngBounds $viewport)
    {
        $this->viewport = $viewport;

        return $this;
    }

    /**
     * @return LatLngBounds
     */
    public function getViewport()
    {
        return $this->viewport;
    }
}
