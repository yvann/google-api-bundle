<?php

namespace Yvann\GoogleAPIBundle\Model\Places;

/**
 * Represents a single review of a place.
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#PlaceReview
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class PlaceReview
{
    /**
     * @var PlaceAspectRatings The aspects rated by the review. The ratings on a scale of 0 to 3.
     */
    protected $aspects;

    /**
     * @var string The name of the reviewer.
     */
    protected $authorName;

    /**
     * @var string A link to the reviewer's profile. This will be undefined when the reviewer's profile is unavailable.
     */
    protected $authorUrl;

    /**
     * @var string The text of a review.
     */
    protected $text;

    /**
     * @var \DateTime The date that the review was submitted
     */
    protected $createdAt;

    public function __construct()
    {
        $this->aspects = new PlaceAspectRatings();
    }

    /**
     * @param  mixed       $aspects
     * @return PlaceReview
     */
    public function setAspects(PlaceAspectRatings $aspects)
    {
        $this->aspects = $aspects;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAspects()
    {
        return $this->aspects;
    }

    /**
     * @param  string      $authorName
     * @return PlaceReview
     */
    public function setAuthorName($authorName)
    {
        $this->authorName = $authorName;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorName()
    {
        return $this->authorName;
    }

    /**
     * @param  string      $authorUrl
     * @return PlaceReview
     */
    public function setAuthorUrl($authorUrl)
    {
        $this->authorUrl = $authorUrl;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorUrl()
    {
        return $this->authorUrl;
    }

    /**
     * @param  string      $text
     * @return PlaceReview
     */
    public function setText($text)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param  \DateTime   $createdAt
     * @return PlaceReview
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }
}
