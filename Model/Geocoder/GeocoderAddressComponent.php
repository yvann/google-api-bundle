<?php

namespace Yvann\GoogleAPIBundle\Model\Geocoder;

/**
 * A single address component within a GeocoderResult. A full address may consist of multiple address components.
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#GeocoderAddressComponent
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class GeocoderAddressComponent
{
    /**
     * @var string The full text of the address component
     */
    protected $longName;

    /**
     * @var string The abbreviated, short text of the given address component
     */
    protected $shortName;

    /**
     * @var array An array of strings denoting the type of this address component.
     */
    protected $types = [];

    /**
     * @param  string                   $longName
     * @return GeocoderAddressComponent
     */
    public function setLongName($longName)
    {
        $this->longName = $longName;

        return $this;
    }

    /**
     * @return string
     */
    public function getLongName()
    {
        return $this->longName;
    }

    /**
     * @param  string                   $shortName
     * @return GeocoderAddressComponent
     */
    public function setShortName($shortName)
    {
        $this->shortName = $shortName;

        return $this;
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        return $this->shortName;
    }

    /**
     * @param  array                    $types
     * @return GeocoderAddressComponent
     */
    public function setTypes(array $types)
    {
        $this->types = [];
        foreach ($types as $type) {
            $this->addType($type);
        }

        return $this;
    }

    /**
     * @param $type
     * @return GeocoderAddressComponent
     */
    public function addType($type)
    {
        $this->types[] = $type;

        return $this;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @return bool
     */
    public function hasType($type)
    {
        return in_array($type, $this->types);
    }
}
