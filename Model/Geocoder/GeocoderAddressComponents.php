<?php

namespace Yvann\GoogleAPIBundle\Model\Geocoder;

use \PhpCollection\Sequence;

/**
 * Defines a collection of GeocoderAddressComponent
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#GeocoderAddressComponent
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class GeocoderAddressComponents extends Sequence
{
}
