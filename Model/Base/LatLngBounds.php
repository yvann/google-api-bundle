<?php

namespace Yvann\GoogleAPIBundle\Model\Base;

/**
 * A LatLngBounds instance represents a rectangle in geographical coordinates, including one that crosses the 180 degrees longitudinal meridian.
 *
 * @see https://developers.google.com/maps/documentation/javascript/3.exp/reference#LatLngBounds
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class LatLngBounds
{
    /**
     * @var LatLng South-West LatLng corner
     */
    protected $southWest;

    /**
     * @var LatLng North-East LatLng corner
     */
    protected $northEast;

    /**
     * @param LatLng $southWest
     * @param LatLng $northEast
     */
    public function __construct(LatLng $southWest = null, LatLng $northEast = null)
    {
        !$southWest ?: $this->setSouthWest($southWest);
        !$northEast ?: $this->setNorthEast($northEast);
    }

    /**
     * @param  LatLng       $southWest
     * @return LatLngBounds
     */
    public function setSouthWest(LatLng $southWest)
    {
        $this->southWest = $southWest;

        return $this;
    }

    /**
     * @return LatLng
     */
    public function getSouthWest()
    {
        return $this->southWest;
    }

    /**
     * @param  LatLng       $southWest
     * @return LatLngBounds
     */
    public function setNorthEast(LatLng $northEast)
    {
        $this->northEast = $northEast;

        return $this;
    }

    /**
     * @return LatLng
     */
    public function getNorthEast()
    {
        return $this->northEast;
    }
}
