<?php

namespace Yvann\GoogleAPIBundle\Model\Base;

/**
 * A LatLng is a point in geographical coordinates: latitude and longitude.
 *
 * @see https://developers.google.com/maps/documentation/javascript/reference#LatLng
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
class LatLng implements LatLngInterface
{
    /**
     * @var float Latitude ranges between -90 and 90 degrees, inclusive
     */
    protected $latitude;

    /**
     * @var float Longitude ranges between -180 and 180 degrees, inclusive
     */
    protected $longitude;

    /**
     * @param null|float $latitude
     * @param null|float $longitude
     */
    public function __construct($latitude = null, $longitude = null)
    {
        !$latitude ?: $this->setLatitude($latitude);
        !$longitude ?: $this->setLongitude($longitude);
    }

    /**
     * @param float $latitude
     *
     * @return LatLng
     */
    public function setLatitude($latitude)
    {
        if (is_numeric($latitude)) {
            $this->latitude = $latitude;
        } else {
            throw new \InvalidArgumentException('The latitude must be a numeric value');
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param float $longitude
     *
     * @return LatLng
     */
    public function setLongitude($longitude)
    {
        if (is_numeric($longitude)) {
            $this->longitude = $longitude;
        } else {
            throw new \InvalidArgumentException('The longitude must be a numeric value');
        }

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
}
