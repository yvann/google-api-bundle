<?php

namespace Yvann\GoogleAPIBundle\Model\Base;

/**
 * A LatLng is a point in geographical coordinates: latitude and longitude.
 *
 * @see https://developers.google.com/maps/documentation/javascript/reference#LatLng
 * @author Yvann Boucher <yvann.boucher@gmail.com>
 */
interface LatLngInterface
{
    /**
     * @return null|float
     */
    public function getLatitude();

    /**
     * @return null|float
     */
    public function getLongitude();
}
